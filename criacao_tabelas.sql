/* Script criação de tabelas */
 
DROP DATABASE basePBD;
CREATE DATABASE basePBD;
use basePBD;
 
/* Tabela Endereço */
CREATE TABLE endereco(codigo INT NOT NULL AUTO_INCREMENT PRIMARY KEY, rua VARCHAR(255) NOT NULL, bairro VARCHAR(255), numero BIGINT NOT NULL,complemento VARCHAR(255), cidade VARCHAR(255));

/* Tabela Paciente */
CREATE TABLE paciente(nome varchar(255) NOT NULL, cpf VARCHAR(11) NOT NULL PRIMARY KEY, data_nasc DATE,telefone BIGINT, codigo_endereco INT,CONSTRAINT fk_codigo_endereco_paciente FOREIGN KEY (codigo_endereco) REFERENCES endereco(codigo));

/* Tabela Formação */
CREATE TABLE formacao(codigo INT NOT NULL AUTO_INCREMENT PRIMARY KEY, nome varchar(255) NOT NULL, descricao varchar(255));

/* Tabela Medico */
CREATE TABLE medico(nome VARCHAR(255) NOT NULL,cpf VARCHAR(11) NOT NULL UNIQUE, data_nasc DATE, cremer BIGINT NOT NULL PRIMARY KEY, formacao_principal INT  NOT NULL, codigo_endereco INT,tel_prof BIGINT,tel_pes BIGINT,CONSTRAINT fk_formacao_principal FOREIGN KEY (formacao_principal) REFERENCES formacao(codigo), CONSTRAINT fk_codigo_endereco_medico FOREIGN KEY (codigo_endereco) REFERENCES endereco(codigo));

/* Tabela Formação Auxiliar */
CREATE TABLE formacao_auxiliar(codigo_medico BIGINT NOT NULL,CONSTRAINT fk_codigo_medico FOREIGN KEY(codigo_medico) REFERENCES medico(cremer),codigo_formacao INT NOT NULL, CONSTRAINT fk_codigo_formacao FOREIGN KEY(codigo_formacao) REFERENCES formacao(codigo), CONSTRAINT pk_formacao_auxiliar PRIMARY KEY CLUSTERED(codigo_medico,codigo_formacao));

/* Tabela Função Funcionario */
CREATE TABLE funcao_funcionario(codigo INT NOT NULL AUTO_INCREMENT PRIMARY KEY,funcao VARCHAR(255) NOT NULL);

/* Tabela Hospital */
CREATE TABLE hospital(codigo INT NOT NULL AUTO_INCREMENT PRIMARY KEY, nome VARCHAR(255) NOT NULL, cart_trab_diretor BIGINT,leitos_vagos INT NOT NULL DEFAULT 0, num_leitos INT NOT NULL DEFAULT 0, codigo_endereco INT,CONSTRAINT fk_codigo_endereco_hospital FOREIGN KEY (codigo_endereco) REFERENCES endereco(codigo));

/* Tabela Funcionario */
CREATE TABLE funcionario(nome VARCHAR(255) NOT NULL, data_nasc DATE, cpf VARCHAR(11) NOT NULL UNIQUE, cart_trab BIGINT NOT NULL UNIQUE PRIMARY KEY,funcao INT NOT NULL, codigo_supervisor BIGINT, codigo_hospital INT NOT NULL, codigo_endereco INT,CONSTRAINT fk_funcao_funcionario FOREIGN KEY (funcao) REFERENCES funcao_funcionario(codigo),CONSTRAINT fk_codigo_hospital_funcionario FOREIGN KEY (codigo_hospital) REFERENCES hospital(codigo),CONSTRAINT fk_codigo_supervisor FOREIGN KEY (codigo_supervisor) REFERENCES funcionario(cart_trab),CONSTRAINT fk_codigo_endereco_funcionario FOREIGN KEY (codigo_endereco) REFERENCES endereco(codigo));

/* Tabela Enfermeiro */
CREATE TABLE enfermeiro(cart_trab BIGINT NOT NULL UNIQUE, codigo_corem BIGINT NOT NULL UNIQUE,CONSTRAINT fk_cart_trab_enfermeiro FOREIGN KEY (cart_trab) REFERENCES funcionario(cart_trab));

/* Tabela Atendente */
CREATE TABLE atendente(cart_trab BIGINT NOT NULL UNIQUE, lingua_1 VARCHAR(30) NOT NULL,lingua_2 VARCHAR(30), CONSTRAINT fk_cart_trab_atendente FOREIGN KEY (cart_trab) REFERENCES funcionario(cart_trab));

/* Tabela Diretor */
CREATE TABLE diretor(cart_trab BIGINT NOT NULL UNIQUE, data_inicio DATE NOT NULL, data_fim DATE, CONSTRAINT fk_cart_trab_diretor FOREIGN KEY (cart_trab) REFERENCES funcionario(cart_trab));

/* Tabela Leitos*/
/* Lógica */
/* Utilizado o numero do leito e codigo do hospital como chave composta */
/* Porém ainda se fez necessário o codigo do leito para ser utilizado na hospitalização
/* onde se sabe que esse leito especifico possui tal código */
CREATE TABLE leitos(codigo INT NOT NULL AUTO_INCREMENT UNIQUE,numero BIGINT NOT NULL, tipo VARCHAR(255), vago CHAR NOT NULL DEFAULT "S" ,codigo_hospital INT NOT NULL, CONSTRAINT fk_codigo_hospital FOREIGN KEY(codigo_hospital) REFERENCES hospital(codigo), CONSTRAINT pk_leitos PRIMARY KEY CLUSTERED(numero,codigo_hospital));

/* Tabela Consulta */
CREATE TABLE consulta(data DATE NOT NULL, hora TIME NOT NULL, cpf_paciente VARCHAR(11) NOT NULL, CONSTRAINT fk_cpf_paciente_consulta FOREIGN KEY (cpf_paciente) REFERENCES paciente(cpf),cremer_medico BIGINT NOT NULL,CONSTRAINT fk_cremer_medico FOREIGN KEY (cremer_medico) REFERENCES medico(cremer),codigo_hospital INT NOT NULL,CONSTRAINT fk_codigo_hospital_consulta FOREIGN KEY (codigo_hospital) REFERENCES hospital(codigo),assunto VARCHAR(255) NOT NULL,CONSTRAINT pk_consulta PRIMARY KEY CLUSTERED(data,hora));

/* Tabela Hospitalização */
CREATE TABLE hospitalizacao(codigo INT NOT NULL AUTO_INCREMENT PRIMARY KEY, cpf_paciente VARCHAR(11) NOT NULL,CONSTRAINT fk_cpf_paciente_hospitalizado FOREIGN KEY (cpf_paciente) REFERENCES paciente(cpf), codigo_leito INT NOT NULL, CONSTRAINT fk_codigo_leito FOREIGN KEY (codigo_leito) REFERENCES leitos(codigo), codigo_enfermeiro BIGINT NOT NULL, CONSTRAINT fk_codigo_enfermeiro FOREIGN KEY (codigo_enfermeiro) REFERENCES enfermeiro(codigo_corem), obs VARCHAR(255), data_entrada DATE NOT NULL, data_saida DATE);

ALTER TABLE hospital ADD CONSTRAINT fk_diretor_hospital FOREIGN KEY (cart_trab_diretor) REFERENCES diretor(cart_trab);
