/* Querys sql */
 
/* SELECIONA MÉDICOS COM FORMAÇÃO PRINCIPAL OU FORMACAO AUXILIAR EM CARDIOLOGIA */

SELECT DISTINCT m.nome AS Nome, f.nome AS Formação FROM formacao AS f,(SELECT m.cremer, m.nome, m.formacao_principal FROM medico AS m, formacao_auxiliar AS fa WHERE m.formacao_principal = 1 OR (fa.codigo_formacao = 1 AND fa.codigo_medico = m.cremer)) AS m WHERE f.codigo = 1;

/* SELECIONA A QUANTIDADE DE LEITOS OCUPADOS DE CADA HOSPITAL */

SELECT SUM(num_leitos - leitos_vagos) AS Leitos_Ocupados, nome AS Hospital FROM hospital where codigo = 4;

/* SELECIONA TODOS ENFERMEIROS QUE TRABALHAM NO HOSPITAL X, O MESMO PODE SER UTILIZADO PARA BUSCAR ATENDENTE(funcao = 2) OU DIRETOR(funcao = 3) */

SELECT f.nome AS Nome, f.cart_trab AS Carteira, f.codigo_hospital AS Hospital FROM funcionario AS f WHERE f.funcao = 1 AND f.codigo_hospital = 2;

/* SELECIONA ENFERMEIROS, SEU CODIGO COREM E OS LEITOS VAGOS NO HOSPITAL EM QUE O ENFERMEIRO TRABALHA */

SELECT f.nome AS Enfermeiro, e.codigo_corem AS Corem, l.codigo AS Codigo_leito FROM enfermeiro AS e, funcionario AS f , leitos AS l WHERE f.funcao = 1 AND f.codigo_hospital = 5 AND e.cart_trab = f.cart_trab AND l.codigo_hospital = f.codigo_hospital ORDER BY nome;

/* LISTA ENFERMEIROS RESPONSAVEIS PELOS LEITOS NO HOSPITAL X */

SELECT DISTINCT fe.nome AS Enfermeiro_responsável, fe.codigo_corem AS Corem, l.numero AS N_Leito, h.nome AS Hospital FROM (SELECT nome FROM hospital WHERE codigo = 2) AS h,hospitalizacao AS ho,(SELECT l.numero, l.codigo FROM leitos AS l WHERE l.vago = 'N' AND l.codigo_hospital = 2) AS l, (SELECT f.nome,e.codigo_corem FROM enfermeiro AS e, funcionario AS f WHERE f.cart_trab = e.cart_trab AND f.codigo_hospital = 2) AS fe WHERE l.codigo = ho.codigo_leito AND ho.codigo_enfermeiro = fe.codigo_corem;

