/* Script de atualiza de tabelas */
use basePBD;
 
/* ATUALIZA CAMPO CODIGO_SUPERVISOR UTILIZANDO O CPF DO SUPERVISOR E COMO CLAUSULA O HOSPITAL A QUE O FUNCIONARIO ESTÁ ASSOCIADO */

UPDATE funcionario AS f,(SELECT * FROM funcionario WHERE cpf = 32467463231 OR cpf = 14636478623 OR cpf = 34632721221 OR cpf = 73829408751 OR cpf = 01212567892) AS s SET f.codigo_supervisor = s.cart_trab WHERE s.cart_trab != f.cart_trab AND s.codigo_hospital = f.codigo_hospital;

/* ADICIONA DIRETOR AO HOSPITAL REFERENTE */

UPDATE hospital AS h,(SELECT cart_trab,codigo_hospital FROM funcionario WHERE funcao = 3) AS f SET h.cart_trab_diretor = f.cart_trab WHERE h.codigo = f.codigo_hospital;

/* ATUALIZA LEITOS OCUPADOS */

UPDATE leitos AS l,(SELECT codigo_leito FROM hospitalizacao) AS h SET l.vago = 'N' WHERE h.codigo_leito = l.codigo;
UPDATE hospitalizacao SET data_saida = now() where codigo = 4;
UPDATE leitos AS l,(SELECT codigo_leito, data_saida FROM hospitalizacao) AS h SET l.vago = 'S' WHERE h.codigo_leito = l.codigo AND h.data_saida IS NOT NULL;

/* ATUALIZA LEITOS NOS HOSPITAIS */

UPDATE hospital AS h SET h.leitos_vagos = (SELECT count(vago) FROM leitos where vago = 'S' AND codigo_hospital = h.codigo);
UPDATE hospital AS h SET h.num_leitos = (SELECT count(*) FROM leitos where codigo_hospital = h.codigo);