/* Script de inserção de dados nas tabelas */
use basePBD;
  
/* INSERÇÃO TABELA ENDEREÇO */

INSERT INTO endereco(rua,bairro,numero,cidade) VALUES ('Ernani Osmar Blass','Centro',290,'Pelotas'),('Dom Pedro II','Centro',800,'Pelotas'),("General Osório",'Centro',940,'Pelotas'),('Gonçalves Chaves','Centro',860,'Pelotas'),('Barroso','Centro',1070,'Pelotas');

/* INSERÇÃO TABELA HOSPITAL */

INSERT INTO hospital(nome,codigo_endereco)VALUES('Hospital 1',4),('Hospital 2',3),('Hospital 3',4),('Hospital 4',1),('Hospital 5',2);

/* INSERÇÃO TABELA FUNÇÃO FUNCIONARIO */
INSERT INTO funcao_funcionario(funcao)VALUES('enfermeiro'),('atendente'),('diretor');

/* INSERÇÃO TABELA FUNCIONÁRIO */
/* =============================================================================================== */
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 1','1900-06-29',01234567892,3278423,1,null,2,5);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 2','1967-4-3',14636478623,327894237,3,null,3,1);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 3','1290-5-2',32467463231,324234234,3,null,2,4);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 4','1987-5-2',34632721221,346231932,3,null,4,4);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 5','1252-6-2',73829408751,37409183345,3,null,1,5);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 6','1920-06-29',01212567892,388888423,3,null,5,1);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 7','1968-4-3',46361114786,30207894237,2,null,3,2);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 8','1291-5-2',32000746323,322224234234,1,null,2,5);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 9','1927-5-2',34632000721,3462319320,1,null,4,2);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 10','1252-6-2',73545212677,0000083345,1,null,1,1);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 11','1910-06-29',75839204856,90876482393,2,null,2,2);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 12','1967-4-3',09877655431,90000000492,2,null,2,1);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 13','1290-2-2',83493027520,89034567282,1,null,5,4);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 14','1287-5-2',19405728491,16273849502,1,null,3,4);
INSERT INTO funcionario(nome,data_nasc,cpf,cart_trab,funcao,codigo_supervisor,codigo_hospital,codigo_endereco)VALUES('Nome 15','1251-6-2',34623465423,123566434212,2,null,2,5);


/* INSERÇÃO EM TABELAS DE ESPECIALIZAÇÃO DE FUNCIONÁRIO */
/* ====================================================================================================================== */

/* Realiza inserção tabela enfermeiro, onde funcao associada ao funcionario é enfermeiro e cpf = passado por parametro */
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 73545212677),457892);
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 1234567892),454782);
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 34632000721),358982);
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 19405728491),358231);
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 83493027520),688231);
INSERT INTO enfermeiro (cart_trab,codigo_corem)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'enfermeiro') AND cpf = 32000746323),488931);

/* Realiza inserção tabela atendente, onde funcao associada ao funcionario é atendente e cpf = passado por parametro */
INSERT INTO atendente(cart_trab,lingua_1,lingua_2)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'atendente') AND cpf = 46361114786),'Inglês','Francês');
INSERT INTO atendente(cart_trab,lingua_1,lingua_2)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'atendente') AND cpf = 09877655431),'Inglês','Alemão');
INSERT INTO atendente(cart_trab,lingua_1)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'atendente') AND cpf = 34623465423),'Espanhol');
INSERT INTO atendente(cart_trab,lingua_1,lingua_2)VALUES((SELECT cart_trab FROM funcionario WHERE funcao = (select codigo from funcao_funcionario where funcao = 'atendente') AND cpf = 75839204856),'Espanhol','Mandarin');

/* Cuidar na hora de inserir dados para verificar se já não possui diretor associado para o hospítal em questão */
INSERT INTO diretor(cart_trab,data_inicio)VALUES((select cart_trab from funcionario as f where f.funcao = (select codigo from funcao_funcionario where funcao = 'diretor') AND f.cpf=32467463231),now());
INSERT INTO diretor(cart_trab,data_inicio)VALUES((select cart_trab from funcionario as f where f.funcao = (select codigo from funcao_funcionario where funcao = 'diretor') AND f.cpf=14636478623),'2016-3-10');
INSERT INTO diretor(cart_trab,data_inicio)VALUES((select cart_trab from funcionario as f where f.funcao = (select codigo from funcao_funcionario where funcao = 'diretor') AND f.cpf=34632721221),'2017-5-12');
INSERT INTO diretor(cart_trab,data_inicio)VALUES((select cart_trab from funcionario as f where f.funcao = (select codigo from funcao_funcionario where funcao = 'diretor') AND f.cpf=73829408751),'2018-01-20');
INSERT INTO diretor(cart_trab,data_inicio)VALUES((select cart_trab from funcionario as f where f.funcao = (select codigo from funcao_funcionario where funcao = 'diretor') AND f.cpf=01212567892),'2013-10-10');

/* ====================================================================================================================== */

/* INSERÇÃO PACIENTES */

INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 1',12378287783,'2000-01-01',998302843,4);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 2',15687563201,'1920-01-01',987561027,3);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 3',25468721893,'1865-10-08',986204510,4);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 4',33658966432,'1990-01-12',752013427,2);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 5',12589224156,'1996-12-01',782526420,5);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 6',85621458962,'2001-01-01',455665421,4);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 7',64286125382,'1921-01-01',912212322,3);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 8',15884628281,'1866-10-08',933156821,4);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 9',23032164580,'1991-01-12',752255812,5);
INSERT INTO paciente(nome,cpf,data_nasc,telefone,codigo_endereco)VALUES('Paciente 10',1336459982,'1995-12-01',796626526,1);

/* INSERÇÃO TABELA FORMAÇÃO */

INSERT INTO formacao(nome,descricao)VALUES('Cardiologia','Aborda as doenças relacionadas com o coração e sistema vascular');
INSERT INTO formacao(nome,descricao)VALUES('Pediatria','É a parte da medicina que estuda e trata crianças');
INSERT INTO formacao(nome,descricao)VALUES('Radiologia','Realização e interpretação de exames de imagem como raio-X, ultrassonografia, Doppler colorido, Tomografia Computadorizada, Ressonância Magnética, entre outros');
INSERT INTO formacao(nome,descricao)VALUES('Psiquiatria','É a parte da medicina que previne e trata ao transtornos mentais e comportamentais');
INSERT INTO formacao(nome,descricao)VALUES('Neurologia','É a parte da medicina que estuda e trata o sistema nervoso');
INSERT INTO formacao(nome,descricao)VALUES('Cirurgia Torácica','Atua na cirurgia da caixa torácica e vias aéreas');

/* INSERÇÃO TABELA MÉDICO */

INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 1','82948192376','1988-2-22',92034829,3,2,9987385092,39957923);
INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 2','12455268321','1968-11-2',12456248,2,5,5266642155,37315468);
INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 3','25852332154','1985-8-11',85622819,5,3,8856564124,39821648);
INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 4','89612564858','1977-7-13',75220136,4,1,8254628815,38967221);
INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 5','56624255181','1975-3-2',85162420,1,4,9845762182,39111567);
INSERT INTO medico(nome,cpf,data_nasc,cremer,formacao_principal,codigo_endereco,tel_prof,tel_pes)VALUES('Médico 6','12585467218','1963-7-25',53169729,6,1,5897385223,37643618);

/* INSERÇÃO TABELA FORMAÇÃO AUXILIAR */

INSERT INTO formacao_auxiliar(codigo_medico,codigo_formacao)VALUES(75220136,3),(75220136,5);
INSERT INTO formacao_auxiliar(codigo_medico,codigo_formacao)VALUES(85622819,4),(85622819,2);
INSERT INTO formacao_auxiliar(codigo_medico,codigo_formacao)VALUES(85162420,3),(85162420,6);
INSERT INTO formacao_auxiliar(codigo_medico,codigo_formacao)VALUES(92034829,1),(92034829,5);
INSERT INTO formacao_auxiliar(codigo_medico,codigo_formacao)VALUES(53169729,2),(53169729,4);

/* INSERÇÃO TABELA CONSULTA */

INSERT INTO consulta(cpf_paciente,cremer_medico,codigo_hospital,assunto,data,hora)VALUES((select cpf from paciente where nome = 'Paciente 2'),(select m.cremer from medico as m where m.formacao_principal = 2),(select codigo from hospital where nome='Hospital 2'),'Dor de ouvido','1996-9-27',now());
INSERT INTO consulta(cpf_paciente,cremer_medico,codigo_hospital,assunto,data,hora)VALUES((select cpf from paciente where nome = 'Paciente 3'),(select cremer from medico where nome='Médico 3'),(select codigo from hospital where nome='Hospital 4'),'Cirurgia coração','2009-2-21',now());
INSERT INTO consulta(cpf_paciente,cremer_medico,codigo_hospital,assunto,data,hora)VALUES((select cpf from paciente where nome = 'Paciente 4'),(select cremer from medico where nome='Médico 5'),(select codigo from hospital where nome='Hospital 1'),'Quadro depressivo','2013-5-29',now());
INSERT INTO consulta(cpf_paciente,cremer_medico,codigo_hospital,assunto,data,hora)VALUES((select cpf from paciente where nome = 'Paciente 5'),(select cremer from medico where nome='Médico 1'),(select codigo from hospital where nome='Hospital 3'),'Raio-x','2007-9-3',now());
INSERT INTO consulta(cpf_paciente,cremer_medico,codigo_hospital,assunto,data,hora)VALUES((select cpf from paciente where nome = 'Paciente 1'),(select cremer from medico where formacao_principal=2),(select codigo from hospital where nome='Hospital 5'),'Virose',now(),now());

/* INSERÇÃO TABELA LEITOS */

INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(20,'compartilhado',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(24,'comum',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(2,'comum',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(2,'Pós cirurgico',5);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(10,'comum',2);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(15,'compartilhado',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(20,'comum',3);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(20,'compartilhado',2);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(8,'Pós cirurgico',1);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(5,'comum',5);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(30,'compartilhado',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(12,'comum',3);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(18,'comum',2);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(39,'compartilhado',1);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(32,'compartilhado',1);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(412,'Pós cirurgico',2);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(549,'compartilhado',3);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(60,'comum',2);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(8,'compartilhado',4);
INSERT INTO leitos(numero,tipo,codigo_hospital)VALUES(45,'compartilhado',3);

/* INSERÇÃO TABELA HOSPITALIZAÇÃO */

INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(15687563201,5,488931,'Ta ruim na fita',now());
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(33658966432,9,457892,'Ta no farelo da bolacha','2018-1-10');
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(25468721893,19,358982,'Está na capa da gaita','2007-1-8');
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(12378287783,15,457892,'Fazendo a curva do cabo da boa esperança','2010-11-23');
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(12589224156,10,688231,'Louco para abotoar o paletó','2015-2-23');
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(64286125382,16,488931,"Passagem rápida",'2017-9-12');
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(23032164580,13,454782,"Deve ficar em observação constante",now());
INSERT INTO hospitalizacao(cpf_paciente,codigo_leito,codigo_enfermeiro,obs,data_entrada)VALUES(1336459982,3,358982,"Apresenta sintomas de ataque cardíaco",now());